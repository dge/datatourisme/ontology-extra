CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [En cours]

## [1.0.0] - 2022-01-27

### Ajouts
- Publication initiale
